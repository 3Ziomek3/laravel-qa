<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Question;

class FavoriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Question $question)
    {
        $question->increment('votes_count');

        $question->favorites()->attach(auth()->id());

        if(request()->expectsJson())
        {
            return response()->json(NULL, 204);
        }

        return back();
    }
    public function destroy(Question $question)
    {
        $question->decrement('votes_count');

        $question->favorites()->detach(auth()->id());

        if(request()->expectsJson())
        {
            return response()->json(NULL, 204);
        }

        return back();
    }
}