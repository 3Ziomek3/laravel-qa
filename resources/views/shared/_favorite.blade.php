<a href="" title="Click to mark as favourite question ( Click again to undo )"
   class="favourite mt-2 {{ Auth::guest() ? '' : ($model->is_favorited ? 'favorited' : '') }}"
   onclick="event.preventDefault(); document.getElementById('favorite-question-{{ $model->id }}').submit();">
        <span class="favourites-count">
            <i class="fas fa-star fa-2x"></i>
        </span>
        {{ $model->votes_count }}
</a>
<form id="favorite-question-{{ $model->id }}" action="/questions/{{ $model->id }}/favorite" method="post">
    @csrf
    @if($model->is_favorited)
        @method('DELETE')
    @endif
</form>